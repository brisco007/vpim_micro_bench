#include <assert.h>
#include <dpu.h>
#include <dpu_log.h>
#include <stdio.h>
#include <time.h>


#ifndef DPU_BINARY
#define DPU_BINARY "./helloworld"
#endif

int main(void) {
  struct dpu_set_t set, dpu;
   struct timespec start, end;
    double elapsed;
 
  DPU_ASSERT(dpu_alloc(60, NULL, &set));

    clock_gettime(CLOCK_MONOTONIC, &start);
  DPU_ASSERT(dpu_load(set, DPU_BINARY, NULL));
   clock_gettime(CLOCK_MONOTONIC, &end);
  elapsed = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1000000000.0;
  printf("Execution time dpu_load : %.10f seconds\n", elapsed);

  DPU_ASSERT(dpu_launch(set, DPU_SYNCHRONOUS));

  DPU_FOREACH(set, dpu) {
    DPU_ASSERT(dpu_log_read(dpu, stdout));
  }

  DPU_ASSERT(dpu_free(set));

  return 0;
}