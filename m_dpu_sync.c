#include <dpu.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#ifndef DPU_BINARY
#define DPU_BINARY "checksum"
#endif

/* Size of the buffer for which we compute the checksum: 64KBytes. */
#define BUFFER_SIZE (1 << 16)

void populate_mram(struct dpu_set_t set, uint32_t nr_dpus) {
  struct dpu_set_t dpu;
  uint32_t each_dpu;
  uint8_t *buffer = malloc(BUFFER_SIZE * nr_dpus);




  DPU_FOREACH(set, dpu, each_dpu) {
    for (int byte_index = 0; byte_index < BUFFER_SIZE; byte_index++) {
      buffer[each_dpu * BUFFER_SIZE + byte_index] = (uint8_t)byte_index;
    }
    buffer[each_dpu * BUFFER_SIZE] += each_dpu; // each dpu will compute a different checksum
    DPU_ASSERT(dpu_prepare_xfer(dpu, &buffer[each_dpu * BUFFER_SIZE]));
  }


  DPU_ASSERT(dpu_push_xfer(set, DPU_XFER_TO_DPU, "buffer", 0, BUFFER_SIZE, DPU_XFER_DEFAULT));



  free(buffer);
}

void print_checksums(struct dpu_set_t set, uint32_t nr_dpus) {
  struct dpu_set_t dpu;
  uint32_t each_dpu;
  uint32_t checksums[nr_dpus];

    


  DPU_FOREACH(set, dpu, each_dpu) {

    DPU_ASSERT(dpu_prepare_xfer(dpu, &checksums[each_dpu]));

  
  }

  DPU_ASSERT(dpu_push_xfer(set, DPU_XFER_FROM_DPU, "checksum", 0, sizeof(uint32_t), DPU_XFER_DEFAULT));

  DPU_FOREACH(set, dpu, each_dpu) {
    printf("[%u] computed checksum = 0x%08x\n", each_dpu, checksums[each_dpu]);
  }
}

int main() {
  struct dpu_set_t set, dpu;
  uint32_t nr_dpus;

   struct timespec start, end;
    double elapsed;

  DPU_ASSERT(dpu_alloc(60, NULL, &set));
  DPU_ASSERT(dpu_load(set, DPU_BINARY, NULL));
    DPU_ASSERT(dpu_get_nr_dpus(set, &nr_dpus));

       
  populate_mram(set, nr_dpus);

  DPU_ASSERT(dpu_launch(set, DPU_SYNCHRONOUS));
  print_checksums(set, nr_dpus);

     clock_gettime(CLOCK_MONOTONIC, &start);

  DPU_ASSERT(dpu_sync(set));


  clock_gettime(CLOCK_MONOTONIC, &end);
  elapsed = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1000000000.0;
  printf("Execution time dpu_sync : %.10f seconds\n", elapsed);

  DPU_ASSERT(dpu_free(set));
  return 0;
}